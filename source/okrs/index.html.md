---
layout: markdown_page
title: "Objectives and Key Results (OKRs)"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## What are OKRs?

OKRs are our quarterly goals to execute our [strategy](https://about.gitlab.com/strategy/). To make sure our goals are clearly defined and aligned throughout the organization. For more information see [Wikipedia](https://en.wikipedia.org/wiki/OKR) and [Google Drive](https://docs.google.com/presentation/d/1ZrI2bP-XKEWDWsT-FLq5piuIwl84w9cYH1tE3X5oSUY/edit) (GitLab internal). The OKRs are our quarterly goals

## Format

Before the quarter:

`Owner: Key Result as a sentence. Metric`

During and after the quarter:

`Owner: Key Result as a sentence. Metric => Result`

- Owner is the title of person that will own the result.
- We use two spaces to indent instead of tabs.
- OKRs start with the owner of the key result. When referring to a team lead we don't write 'lead' since it is shorter and the team goal is the same.
- The key result can link to an issue.
- The metric can link to real time data about the current state.
- The three company/CEO objectives are level 3 headers to provide some visual separation.

## Levels

We only list your key results, these have your (team) name on them.
Your objectives are the key results under which your key results are nested, these should normally be owned by your manager.
We do OKRs up to the team or director level, we don't do [individual OKRs](https://hrblog.spotify.com/2016/08/15/our-beliefs/).
Part of the individual performance review is the answer to: how much did this person contribute to the team key objects?
We have no more than [five layers in our team structure](https://about.gitlab.com/team/structure/).
Because we go no further than the team level we end up with a maximum 4 layers of indentation on this page (this layer count excludes the three company/CEO objectives).
The match of one "nested" key result with the "parent" key result doesn't have to be perfect.
Every team should have at most 9 key results. To make counting easier only mention the team and people when they are the owner.
The advantage of this format is that the OKRs of the whole company will fit on 3 pages, making it much easier to have an overview.

## Updating

The key results are updated continually throughout the quarter when needed.
Everyone is welcome to a suggestion to improve them.
To update: make a merge request and assign it to the CEO.
If you're a [team member](https://about.gitlab.com/team/) or in the [core team](https://about.gitlab.com/core-team/) please post a link to the MR in the #okrs channel and at-mention the CEO.

At the top of the OKRs is a link to the state of the OKRs at the start of the quarter so people can see a diff.

Timeline of how we draft the OKRs:

1. Executive team pushes updates to this page: 4 weeks before the start of the quarter
1. Executive team 90 minute planning meeting: 3 weeks before the start of the quarter
1. Discuss with the board and the teams: 2 weeks before the start of the quarter
1. Executive team 'how to achieve' presentations: 1 week before the start of the quarter
1. Add Key Results to top of 1:1 agenda's: before the start of the quarter
1. Present OKRs at a functional group update: first week of the quarter
1. Present 'how to achieve' at a functional group update: during first three weeks of the quarter
1. Review previous quarter and next during board meeting: after the start of the quarter

## Scoring

It's important to score OKRs after the quarter ends to make sure we celebrate what went well, and learn from what didn't in order to set more effective goals and/or execute better next quarter.

1. Move the current OKRs on this page to an archive page _e.g._ [2017 Q3 OKRs](/okrs/2017-q3/)
1. Add in-line comments for each key result briefly summarizing how much was achieved _e.g._
  * "=> Done"
  * "=> 30% complete"
1. Add a section to the archived page entitled "Retrospective"
1. OKR owners should add a subsection for their role outlining...
  * GOOD
  * BAD
  * TRY
1. Promote the draft OKRs on this page to be the current OKRs

## Critical acclaim

Spontaneous chat messages from team members after introducing this:

- As the worlds biggest OKR critic, This is such a step in the right direction :heart: 10 million thumbs up
- I like it too, especially the fact that it is in one page, and that it stops at the team level.
- I like: stopping at the team level, clear reporting structure that isn't weekly, limiting KRs to 9 per team vs 3 per team and 3 per each IC.
- I've been working on a satirical blog post called called "HOT NEW MANAGEMENT TREND ALERT: RJGs: Really Just Goals" and this is basically that. :wink: Most of these are currently just KPIs but I won't say that too loudly :wink: It also embodies my point from that OKR hit piece: "As team lead, it’s your job to know your team, to keep them accountable to you, and themselves, and to be accountable for your department to the greater company. Other departments shouldn’t care about how you measure internal success or work as a team, as long as the larger agreed upon KPIs are aligned and being met."
- I always felt like OKRs really force every person to limit freedom to prioritize and limit flexibility. These ones fix that!

## No hiring as an objective

To to keep the OKRs succinct and to not confuse goals with the means please don't add hiring as a goal. The hiring plan is kept in the Hiring Forecast doc. Frequently we need to hire people to reach a goal, but hiring is not the goal in and of itself. That being said, hiring, coaching, and retention is the most important activity of any manager.

## Current

These are the OKRs for 2018 Q1.

### Objective 1: Grow Incremental ACV according to plan

* CEO Revenue expansion to 200%
  * Sales: Identify success factors
  * Sales: Do quarterly business reviews for all eligible customers
* CEO: Be at a sales efficiency of 1.0 or higher
  * Marketing: know cost per SQO and customer for each of our campaigns
* CEO: Make sure that 70% of salespeople are at 70% of quota
  * Marketing: make sure each SAL has 10 leads per month
  * Sales: 1 month boot-camp for sales people with rigorous testing


* Support
  * 100% Premium and Ultimate SLA achievement

### Objective 2: Popular next generation product

* CEO: GitLab.com ready for mission critical workloads
  * VPE move .com to GKE with Geo
  * VPE .com available 99.95% and monthly disaster recovery exercises
  * VPE .com speed index < 1.5s for all tested pages
  * VPE: All parts of security active (application, automation, operations, abuse, compliance, vulnerability)
* CEO On track to deliver all features of [complete DevOps](https://about.gitlab.com/2017/10/11/from-dev-to-devops/)
  * VPE: ship faster than before
  * Product plan all features to be done by July 22
  * [One codebase with /ee subdirectory](https://gitlab.com/gitlab-org/gitlab-ee/issues/2952)
* CEO: Make it popular
  * Marketing: Get unique contributors per release to 100
  * Marketing: Increase total users by 5% per month
  * Marketing: Facilitate 100 ambassador events (meetups, presentations)
  * Marketing: be a leader in all relevant analyst reports
  * VPE: Use all of GitLab ourselves (monitoring, release management)


  * VPE
    * Deliver 100% of committed scope for GCP migration milestone #2 by Jan 15
    * Deliver 100% of committed scope for GCP migration milestone #3 by Feb 15
    * Deliver 100% of committed scope for GCP migration milestone #4 by Mar 15
  * UX
    * [Complete Phase Two of design library of assets](https://gitlab.com/groups/gitlab-org/-/epics/29)
    * [Complete comprehensive evaluation of GitLab's Accessibility](https://gitlab.com/groups/gitlab-org/-/epics/31)
    * [Focus UX Research on how to be inclusive of under-represented groups and non-technical users](https://gitlab.com/groups/gitlab-org/-/epics/32)
  * Geo
    * Reduce bug backlog to 0
    * Bugs squashed in month _M_ &ge; bugs reported in month _M-1_
    * Full project-standard unit test coverage for Geo functionality
    * GitLab-QA tests cover all basic Geo functionality as described in GitLab EE documentation
    * Deliver 100% of feature commits in 10.5, 10.6
  * Build
    * Upgrade omnibus and internal omnibus-gitlab Chef
    * Measure upgrade/installation time between two GitLab versions. Collect information on user installation methods.
    * Establish a roadmap for automated vulnerability reporting of shipped libraries
    * Cloud Native Helm charts in Alpha
    * Ship 100% of committed deliverables issues each release
  * Platform
    * Make sure all [Platform backend community contributions](https://gitlab.com/groups/gitlab-org/-/merge_requests?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Community%20Contribution&label_name[]=Platform) are merged, closed, labeled “awaiting feedback”, or taken over by us an in active development
    * Resolve or schedule all AP1, AP2, SP1, and SP2 issues
    * Reduce Platform backend bug backlog by 50%
    * Test backup/restore using GitLab QA
    * Ship [first GraphQL endpoint](https://gitlab.com/gitlab-org/gitlab-ce/issues/34754)
    * Ship 100% of committed deliverables issues each release
  * Discussion
    * Make sure all [Discussion backend community contributions](https://gitlab.com/groups/gitlab-org/-/merge_requests?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Community%20Contribution&label_name[]=Discussion) are merged, closed, labeled “awaiting feedback”, or taken over by us and in active development
    * Resolve or schedule all AP1, AP2, SP1, and SP2 issues
    * Reduce Discussion backend bug backlog by 50%
    * Ship Rails 5 migration
    * Ship 100% of committed deliverables issues each release
  * CI/CD
    * Resolve or schedule all AP1, AP2, SP1, and at least 5 SP2 issues
    * Scalability: Make all CI/CD related data to be stored on Object Storage
    * Performance: Ensure that 95% of pipeline jobs are picked within the first 10s for projects currently not having any jobs running
    * Cost: Move all CI infrastructure to GCP
    * Quality: Test CI workflow with Runner by GitLab QA
    * Ship 100% of committed deliverables issues each release
  * Prometheus
    * Bundle Alertmanager for proactive customer alerting notifications
    * Prometheus deploy for customer apps feature
    * Ship 10 new alerts for monitoring GitLab
    * Instrument gitlab-shell
    * Ship 100% of committed deliverables issues each release
  * Security
    * GDPR: Complete data breach notification policy and data mapping requirements
    * FIPS 140-2: Research requirements and provide guidance to development team to implement
    * SOC 2: Research requirements and build initial roadmap to achieve compliance
    * Complete Remainder of 10 Risk Assessment Actions
    * Automate metrics for vulnerability initiatives: HackerOne, external & internal assessments
  * Database
    * [Make it more difficult for database performance issues to occur](https://gitlab.com/gitlab-com/infrastructure/issues/3474)
    * [Improve workflow / structure of the database team](https://gitlab.com/gitlab-com/infrastructure/issues/3475)
    * [Improve database performance](https://gitlab.com/gitlab-com/infrastructure/issues/3476)
  * Gitaly
    * All migration points complete to Ready-for-Testing state by 7 February
    * All endpoints complete to Opt-In state (including ones currently disabled due to n+1)
    * Hit 1.0 (all endpoints complete to Opt-Out state)
  * Quality
    * Define the architecture of and produce an end-to-end prototype for a self-service metrics generator
  * Edge
    * [De-duplicate at least 5 redundant (feature) tests](https://gitlab.com/gitlab-org/gitlab-ce/issues/39829)
    * Improve at least the [5 longest spec files](https://redash.gitlab.com/queries/15) by at least 30%
    * Investigate code with less than 60% tests coverage and add tests for  at least the [5 most critical files](https://gitlab.com/gitlab-org/gitlab-ce/issues/19412)
    * [Investigate encapsulating instance variables](https://gitlab.com/gitlab-org/gitlab-ce/issues/20045) about the current page in a class
    * [Reduce duplication](https://gitlab.com/gitlab-org/gitlab-ce/issues/31574) in at least 5 forms
    * Solve at least 3 [outstanding performance issues](https://gitlab.com/gitlab-org/gitlab-ce/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=performance&milestone_title=No+Milestone)
    * Complete the work to [make GitLab QA production-ready](https://gitlab.com/gitlab-org/gitlab-qa/issues/126)
    * Define and schedule high-value issues for [improving the staging test environment](https://gitlab.com/gitlab-com/infrastructure/issues/3177)
    * Write 3 tests related to creating and managing Issues
    * Write 3 tests related to CI/CD
  * Frontend
    * Write 120 unit tests to resolve test debt
    * Crush 280 backlogged bugs
    * Ship 100% of committed deliverables issues each release
    * Make sure all [Frontend community contributions](https://gitlab.com/groups/gitlab-org/-/merge_requests?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Community%20Contribution&label_name[]=frontend) are merged, closed, labeled “awaiting feedback”, or taken over by us an in active development

### Objective 3: Great team

* CEO: Hire according to plan
* CEO: Great and diverse hires
  * Global hiring
  * Sourced recruiting 50% of applicants
  * Hired candidates, on average, from areas with a [Rent Index](https://about.gitlab.com/handbook/people-operations/global-compensation/#the-formula) of less than 0.7
* CEO: Keep the handbook up-to-date so we can scale further
  * Handbook first (no presentations about evergreen content)
  * Consolidate and standardize role descriptions


* VPE
  * Consolidate and standardize job descriptions
  * Launch 2018 Q2 department OKRs before EOQ1 2018
  * Set 2018 Q2 hiring plan EOQ1 2018
  * Implement issue taxonomy changes to improve prioritization
  * Record an on-boarding video of how to do a local build and contribute to the GitLab handbook
* Backend
  * Deliver two iterations toward aligning backend teams with the DevOps lifecycle
* Support
  * Define HA Expertise and train 7 support engineers
  * Define Kubernetes Expertise and train 7 support engineers
* UX
  * [Launch Phase II of design.gitlab.com](https://gitlab.com/groups/gitlab-org/-/epics/30)
  * [Write 3 public blog posts about GitLab UX and visual design case studies, best practices, anecdotes, or events](https://gitlab.com/groups/gitlab-org/-/epics/33)
* Quality
  * Document the context and background of release process improvements in the Handbook / Quality page

## Archive

* [2017 Q3](/okrs/2017-q3/)
* [2017 Q4](/okrs/2017-q4/)
