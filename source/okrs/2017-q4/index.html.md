---
layout: markdown_page
title: "2017 Q4 OKRs"
---

## On this page
{:.no_toc}

- TOC
{:toc}

### Objective 1: Grow incremental ACV according to plan.

* CRO: Sales efficiency ratio > 1.2 (IACV / sales+marketing)
  * CRO: Field Sales efficiency ratio > 1.8 (IACV / sales spend)
  * CMO: Marketing efficiency ratio > 3.4 (IACV / marketing spend)
* CMO: Establish credibility and thought leadership with Enterprise Buyers delivering on pipeline generation plan through the development and activation of integrated marketing and sales development campaigns:
  * CMO: Define category strategy, positioning and messaging and plan for activation across the company.
    * CMO: Develop and document messaging framework
    * PMM: Develop and roll out updated pitch and analyst decks
    * PMM: Develop Action Plan for Q1/Q2 activation of the strategy
  * PMM: Develop GTM Strategy for EEU
    * Enable Sales with decks and Early Adopter Program
  * CMO: Continue website redesign iteration to support our awareness and lead generation objectives, accounting for distinct audiences.
  * MSD: achieve target in new inbound opportunity SQL $
  * MSD: achieve target in new outbound opportunity SQL $
  * MSD: achieve new opportunity volume target in strategic Sales segment accounts
  * CMO: Build out Product Marketing function, including hiring and on-boarding three people, updating objectives, process and handbook, and developing cross-functional alignment.
    * PMM: Evolve and deliver updated AE pitch deck messaging and incorporate Sales feedback
      * PMM: Update current “toolchain DevOps” to EE pitch Deck
      * PMM: Create CE to EE “get a demo” Deck
      * PMM: Create CE to EE Pitch Deck
      * PMM: Create SVN to EE pitch Deck
    * PMM: Develop a website Information Architecture (IA) to roll out in 2018
    * PMM: Enhance ROI section of website including adding interstitial to promote individual calculators
    * PMM: Work with content team to deliver 5 customer case studies or customer-centric blog posts.
    * PMM: Evolve EEP vs EES vs CE differentiation messaging and optimize website experience for product sections
* CRO: 100% of new business IACV plan
  * CRO: Sales Health 80% of Reps on Quota
  * CRO: [Increase Average Sales Assisted New Business Deal by 30%](https://na34.salesforce.com/00O61000003jNT6)
    *  RD: 70% of sales assisted deals are EEP
    *  CS: Launch Professional Services - 25% of all deals with subscription ACV of $100,000+ include PS
  * CRO: Decrease Time to close (New Business - Sales Assisted) by 10%
    * Sales Ops: Launch MEDDPIC. Fields 100% filled out for all stage 3+ deals
* CRO: 165% net retention in [Q4 renewal value](https://na34.salesforce.com/00O61000001uN0a)
  * Sales Ops: 95% of subscriptions under $1200 moved to auto-renew
  * CS: 25% of large/strategic accounts on EES trialling EEP with how we expand game plan documented in SFDC
  * CS: Double License Usage within our large and strategic accounts
  * CS: Identity the trigger(s) to purchase for large/strategic accounts
  * CS: Build Customer Advisory board program
* CFO: Efficiency
  * Legal: Publish sales training guide on key legal terms common in deals
  * Legal: Hold one sales team training session published on courses
  * Legal: Implement a legal section for sales team on-boarding
  * Legal: Use GitLab issues to track and collaborate legal matters
  * Controller: Plan for implementing ASC 606 approved
  * Controller: Move billing administration from SMB team to billing specialist
  * Billing Specialist: Create a Zuora training module for quick and efficient training of new sales reps.
  * Analytics: Create a user journey funnel

### Objective 2: Popular next generation product.

* CEO: Next generation
  * VP Product
    * Platform: Make way for a cloud IDE. Multi-file editor with terminal shipped.
    * Discussion: Portfolio management: Epics and roadmap view shipped (as part of EEU)
    * CI/CD: Improve support for Java development lifecycle. 1 more project done.
    * CI/CD: Improve existing features with additional value. 1 feature extended.
    * Prometheus: Make GitLab easier to monitor. [GA Prometheus monitoring of the GitLab server](https://gitlab.com/gitlab-org/gitlab-ce/issues/38568), deprecate InfluxDB.
    * Prometheus: Shift performance monitoring earlier in the pipeline, [detecting regressions prior to merge](https://gitlab.com/gitlab-org/gitlab-ee/issues/3173). Deliver one feature.
    * Prometheus: Complete the feedback loop by [comparing Canary and Stable performance](https://gitlab.com/gitlab-org/gitlab-ee/issues/2594).
  * VP Eng
    * Generate joint PM/Eng plan to update issue taxonomy (labels) to aid prioritization and kickoff implementation => 30% done, MR started, need to socialize and implement next quarter
  * Support Lead
    * 85% Premium Support SLA (up from 68% last quarter) => 83%
    * +5% MoM on all other SLAs => October: Average SLA: 52%. December Average SLA: 60%. 2% MoM Average improvement.
    * One hire through active sourcing => Not hit, but Augie is sourcing EMEA Support Engineers for us!
    * Support Blog post every other week to use as recruiting tool => Done!
  * Design Lead
    * [Document Gitlab UX standards / style guide and communicate  to internal team via UX FGU, the UX Style guide, and within issues](https://gitlab.com/gitlab-org/gitlab-design/issues/67)
    * [Finish Phase 1 of a design library of assets](https://gitlab.com/gitlab-org/gitlab-design/issues/26)
  * Frontend (AC) Lead
    * Write 60 unit tests to resolve test debt in Q4 (evenly distributed throughout the team)
    * Crush 140 bugs this quarter (evenly distributed through the team)
    * Improve codebase by making modules ready for webpack by moving it to our new coding standards ([#38869](https://gitlab.com/gitlab-org/gitlab-ce/issues/38869))
    * Improve performance by making library updates and webpack bundle optimizations ([#39072](https://gitlab.com/gitlab-org/gitlab-ce/issues/39072))
    * Finish conversion from inline icons to SVG Icons to improve performance
  * Frontend (DC) Lead
    * Write 60 unit tests to resolve test debt in Q4 (evenly distributed throughout the team)
    * Crush 140 bugs this quarter (evenly distributed through the team)
    * Refactor the MR discussion in Vue to decrease load times, and increase performance/usability
    * Remove global namespaces, to enable webpack code splitting, which improves performance ([#38869](https://gitlab.com/gitlab-org/gitlab-ce/issues/38869))
  * Director of Backend
    * Author a demo script for use throughout the quarter
    * Expose EE and CE code coverage metrics
    * Assist 1 top tier customer switch to GitLab and ensure P1 bugs/issues get fixed
  * Build Lead
    * Establish baseline metric for install time/ease and come up with a plan to achieve and maintain it
    * Decrease build times from 60 minutes to 30 minutes
    * Create integration test for Mattermost
  * Platform Lead
    * Identify 1 sub-standard area of the code base and raise local unit test coverage up to project level
    * Write integration test for backup/restore
    * Make GitLab QA [test LDAP](https://gitlab.com/gitlab-org/gitlab-qa/issues/3)
    * Resolve or schedule all priority 1 & 2 Platform issues (and groom performance issues)
  * CI/CD Lead
    * Add 1 integration for runners
    * Resolve or schedule all priority 1 & 2 CI/CD issues (and groom performance issues)
    * Reduce amount of system failures to less than 0.1%
    * Improve cost efficiency of CI jobs processing for GitLab.com and GitLab Inc.
  * Discussion Lead
    * Write integration test for squash/rebase
    * Write integration test for protected branches
    * Resolve or schedule all Priority 1 & 2 Discussion issues (and groom performance issues)
  * Prometheus Lead
    * Reach parity with Prometheus metrics for Unicorn, Sidekiq, and gitlab-shell and Deprecate InfluxDB
    * Make Grafana dashboards available for all Prometheus data easy to install for GitLab instances
    * Identify 1 sub-standard area of the code base and raise local unit test coverage up to project level
    * Resolve or schedule all Priority 1 & 2 Prometheus issues (and groom performance issues)
  * Geo Team
    * Make Geo Generally Available
    * Geo performant at GitLab.com scale
    * Manual failover robust in Geo as first step to Disaster Recovery
  * Director of Quality
    * Document what quality means at GitLab on an about page
    * Communicate standard in 3 different ways to internal team
    * Make issue/board scheme change recommendation to allow us to better mine backlog for quality metrics
    * Initiate a project to make quality metrics and charts self-service
    * Initiate a project to allow for UI testing of the web application locally and on CI
  * Edge Lead
    * Ship [large database seeder for developers](https://gitlab.com/gitlab-org/gitlab-ce/issues/28149)
    * Enable [triage](https://gitlab.com/gitlab-org/triage) to be [used for any project](https://gitlab.com/gitlab-org/triage/issues/14#note_41293856)
    * Make GitLab QA [test the Container Registry](https://gitlab.com/gitlab-org/gitlab-qa/issues/49)
    * Make GitLab QA [test upgrade from CE to EE](https://gitlab.com/gitlab-org/gitlab-qa/issues/64)
    * Make GitLab QA [test simple push with PostReceive](https://gitlab.com/gitlab-org/gitlab-qa/issues/53)
    * [De-duplicate at least 5 redundant (feature) tests](https://gitlab.com/gitlab-org/gitlab-ce/issues/39829)
    * Improve at least the [5 longest spec files](https://redash.gitlab.com/queries/15) by at least 30%
    * Investigate code with less than 60% tests coverage and add tests for  at least the [5 most critical files](https://gitlab.com/gitlab-org/gitlab-ce/issues/19412)
    * [Investigate encapsulating instance variables](https://gitlab.com/gitlab-org/gitlab-ce/issues/20045) about the current page in a class
    * [Reduce duplication](https://gitlab.com/gitlab-org/gitlab-ce/issues/31574) in at least 5 forms
    * Solve at least 3 [outstanding performance issues](https://gitlab.com/gitlab-org/gitlab-ce/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=performance&milestone_title=No+Milestone)
  * Director of Security
    * Compliance framework: Detail Breach Notification Policy - [first draft](https://about.gitlab.com/security/#data-breach-notification-policy)
    * Compliance framework: [Create and develop GDPR checklist](https://gitlab.com/gitlab-com/infrastructure/issues/3252)
  * CTO: [Scan source code for security issues](https://gitlab.com/gitlab-org/gitlab-ce/issues/38413 ). Make it work for 3 popular frameworks.
  * CTO: [Less effort to merge CE into EE](https://gitlab.com/gitlab-org/gitlab-ee/issues/2952#note_41016654). 10 times less efforts to merge CE to EE
  * CTO: Start new projects that might materially affect the scope and future of the company.
* CEO: Partnerships
  * VP Product
    * Increase adoption of Kubernetes through integration.
    * CI/CD: Configure review apps and deployment for projects in less than 5 steps.
    * Prometheus: Enable [Prometheus monitoring of Kubernetes clusters]((https://gitlab.com/gitlab-org/gitlab-ce/issues/28916)) with a single click.
    * Platform: Help partners and customers adopt GitLab. Ship authentication and integration requirements.
    * Platform: Ship the GNOME requirements. 5 requirements shipped.
    * At least 3 GNOME projects migrated to GitLab as part of evaluation
    * [AWS QuickStart guide](https://gitlab.com/gitlab-org/gitlab-ce/issues/29199) published
* CEO: Preparing GitLab.com to be mission-critical
  * VP Product
    * Improve GitLab.com subscriptions. Storage size increase per subscription level. Ability to upgrade easily.
  * Build Lead
    * Build GCP deployment mechanism on Kubernetes for the migration
  * Platform Lead
    * Finish [Circuit breakers](https://gitlab.com/gitlab-org/gitlab-ce/issues/37383)
  * Gitaly Lead
    * 100% of Git operations on Gitlab.com go through Gitaly (Gitaly v1.0)
    * Demo Gitaly fast-failure during a file-server outage
    * Generate a project plan for the GCP migration and get approved by EJ and Sid
    * Execute milestone 1 of the GCP migration plan by Dec 15
  * Database Lead
    * Demo restore time < 1 hour
    * Solve 30% of the schema issues identified by Crunchy
    * Database Uptime 99.99% measured in Prometheus
    * SQL timing under 100ms for Issue, MR, project dashboard, and CI pages measured in Prometheus
  * Director of Security
    * Strong security for SaaS and on-premise product. Top 10 actions from risk assessment done and actions for top 10 risks started.
    * HackerOne bug bounty program. Implemented and bounties awarded.
    * Security policies for cloud services and cloud migrations. Policy published and enacted.
* CMO: Build trust of, and preference for GitLab among software developers
  * CMO: Hire Director, DevRel/Community Relations
    * CA: Grow community and increase community engagement. Increase number of new contributors by 10%, increase number of total contributions per release by 5% and increase number of Twitter mentions of GitLab by 10%.
  * PMM: Support field marketing at AWS: Reinvent & KubeCon with booth decks and training
  * MSD: $600K in self serve revenue.
  * MSD: Grow followers by 20% through proactive sharing of useful and interesting information across our social channels.
  * MSD: Grow number of opt-in subscribers to our newsletter by 20%.
* CMO: Generate more company and product awareness including increasing lead over [BitBucket in Google Trends](https://trends.google.com/trends/explore?q=bitbucket,gitlab)
  * MSD: Implement SEO/PPC program to drive increase in number of free trials by 20% compared to last quarter, increase number of contact sales requests by 22% compared to last quarter, increase amount of traffic to about.gitlab.com by 9% compared to last quarter
  * CMO: PR - October Announcements - 10.0, Series C, Wave, CLA
  * CMO: AR - v10 briefing sweep for targeted analysts

### Objective 3: Great team.

* CFO: Improve team productivity
  * Analytics: Data and Analytics vision and plan signed off by executive team
  * Analytics: Real time analytics implemented providing visual representation of the metrics sheet
  * Legal: Create plan for implementing Global Data Protection and Data Privacy Plan
  * Controller: Reduce time to close from 10 days to 9 days.
  * Accounting Manager: Identify and add to the handbook two new accounting policies.
  * Accounting Manager: Create monthly process for BvsA analysis with department budget owners.
* CCO: Create an Exceptional Corporate Culture / Delight Our Employees
  * Launch training for making employment decisions based on the GitLab Values. Launch by November 15th
  * Launch a short, quarterly Employee Pulse Survey. Strive for 80% completion
  * Analyze and make recommendations based off of New Hire Survey and Pulse surveys which will drive future KRs. Have at least 3 areas to improve each quarter. Ideally, we will also have 3 areas to celebrate.
  * Revise the format of the Morning Team Calls to allow for better participation and sharing. Strive for 80% participation
  * Improve use of the GitLab Incentives by 15%. https://about.gitlab.com/handbook/incentives/
  * Iterate on the Performance Review process with at least two changes initiated by end of year.
* CCO: Grow Our Team With A-Players
  * KR: Socialize and grow participation in our Diversity Referral bonuses by 10% (measurement should be made in January as many hires in December don’t start until January, with awareness that the actual bonuses aren’t paid out for 3 months)
  * More sourced recruiting. 20% of total hires
  * Ensure candidates are being interviewed for a fit to our Values as well as ability to do the job, through Manager Training and Follow-up by People Ops.
  * Hire Recruiting Director
  * 90% of all candidates will be advanced through the pipeline within 7 business days in each phase, maximum.
* CCO: Make All of Our Managers More Effective and Successful
  * Provide consistent training to managers on how to manage effectively. Success will mean that there are at least 15 live trainings a year in addition to curated online trainings.
  * Ensure every manager is doing regular 1-on-1 meetings with 2-way feedback. Measure will be seen in Employee Pulse survey, with at least 90% of employees indicating they have received feedback from their manager in the last month.
  * Hire People Business Partners to partner with managers to operate as leadership coaches, performance management advisors, talent scouts, and Culture/Values evangelists. Goal of 2 hires.
* VPE: Build the best, global, diverse engineering, design, and support teams in the developer platform industry
  * Revise hiring plan for Q1 2018 based on Q4 financials and product ambitions => Done
  * Launch 2018 Q1 department OKRs before EOQ4 2017 => Done
  * Hire an additional Director of Engineering => Job posted, pipeline looks decent, but hire not made
  * Hire a production engineers => Job posted, pipeline looks decent, but hire not made
* Support: Grow the support team to better comply with SLAs and cover gitlab.com cases
  * Hire a Services Support Manager => Done, Starting in Feb.
  * Hire an support specialist
  * Hire an EMEA support engineer => Hired AMER as needed.
  * Hire an EMEA/AMER support engineer => Done.
  * Hire an AMER support engineer => Done, Starting Jan 8th.
* UX: Increase the profile of GitLab design and grow the team
  * [Launch first iteration of design.gitlab.com](https://gitlab.com/gitlab-org/gitlab-design/issues/44)
  * [Write 3 public blog posts about GitLab UX and visual design case studies, best practices, anecdotes, or events](https://gitlab.com/gitlab-org/gitlab-design/issues/75)
  * Hire 2 UX designers
  * Hire a junior UX researcher
* Frontend (DC): Hire 3 front end developers
* Frontend (AC): Hire 2 front end developers
* VP of Scaling: Hire an Engineering Manager for the Geo team
* Build
  * Hire 2 developers
  * Hire a senior developer
* CI/CD
  * Hire 3 developers
  * Hire 2 senior developers
* Discussion: Hire 2 developers
* Gitaly: Hire a developer
* Database: Hire a database specialist
* Director of Quality
  * Hire a test automation lead
  * Hire 3 test automation engineers
* Director of Security
  * Hire Security Engineer(s)
  * Hire a Security Specialist Developer

## Retrospective

* CEO
  * GOOD
  * BAD
  * TRY
* VPE
  * GOOD
    * Anecdotally, Engineering had been setting promises and achieving ~20-30% of them in past quarters. It's important to set aggressive but achievable goals that both motivate the team, but also accurately represent our bandwidth to the rest of the organization. Otherwise, the wrong business decisions are made. My goal was to raise our achievement somewhat, which I think we've done. Eventually, I would like to be regularly hitting 70-80% of our OKRs--more when things go spectacularly. But it will take time to dial this is, because we do not want to encourage sandbagging.
    * The focus on hiring velocity and quality improved dramatically. Teams such as design, frontend, and support did a great job, meeting their plan, raising the bar higher, and making the process more efficient.
    * We gave engineering teams many goals that were fully under their control, such as resolving test debt, improving code quality, and improving the hygiene of their backlog and most delivered on these
    * We got 2018 Q1 OKRs drafted before the end of the quarter, which helps with adoption
    * We delivered geo
    * Two revisions of the hiring plan were delivered to the board
    * We kicked off the GCP migration project and delivered a milestone
  * BAD
    * These goals were set in my 2nd week at GitLab, so some missed the mark (which was known to be likely to happen)--I know much more for Q1
    * Some teams lagged behind in hiring, only getting their vacancies up in Late November after being pressed
    * My own hires (some of which were inherited from infrastructure) were not made
    * The late start and holiday season made it very unlikely that some of our hires would be made (but it was important to capture them on the record, rather than leave them off)
    * My goal to enhance our process somehow meandered through the quarter. It started as starting an estimation process, eventually becoming a taxonomy change
    * My time spent with the infrastructure team took the place of several other things I hoped to do--unfortunate, but the right call
  * TRY
    * Anticipate the holiday season next year and front-load hires in Q3
    * Push for all vacancies to be posted in the first full business week of each quarter
    * Assign each team a goal to deliver 100% of commitments for releases (and find a way to measure it)
    * Find a way to incentivize hiring in efficient regions
    * Assign each applicable team a goal to merge a certain amount of contributions from the community
*  Database Lead
  * GOOD
  * BAD
  * TRY
