---
layout: markdown_page
title: "GitLab BV (Belgium) Benefits"
---

Can't find what you're looking for? Try the main [People Operations page](/handbook/people-operations).

## On this page
{:.no_toc}

- TOC
{:toc}

----

## Specific to Belgium based employees

### Ecocheques

Each year all employees will be sent ecocheques no later than July 31. In accordance with the local law these are provided by GitLab at no cost to the individuals. These are currently sent by GitLab's payroll provider in electronic form via a [Sodexo](http://be.benefits-rewards.sodexo.com/) card. A pin for this card will be sent separately. The maximum value is 250 Euros, the amount that will be granted is pro-rated depending on the employee's start date. The reference period is from July 1 of the previous year to June 30 of the year of payment. These ecocheques must be used to purchase ecological products and services; you can always find a current list of what is available on the [National Labor Council](http://www.cnt-nar.be/CAO-ORIG/cao-098-quinquies-(23-05-2017).pdf) website. These ecocheques are exempt from social security contributions and may not be awarded for replacement or conversion of salary.
