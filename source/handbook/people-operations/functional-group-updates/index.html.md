---
layout: markdown_page
title: "Functional Group Updates"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Format

Functional Group Updates are regular updates from a group at GitLab.

1. We'll use Zoom for now, will switch to YouTube Live later.
1. Use this [slide deck](https://docs.google.com/a/gitlab.com/presentation/d/1JYHRhLaO9fMy1Sfr1WDnCPGv6DrlohlpOzs48VvmlQw/edit?usp=sharing) as template to your presentation. Presentations should allow editing (preferred) or commenting from everyone at GitLab.
1. Make sure to add the link of the presentation to the GitLab Availability Calendar invite at least 24 hours _before_ the call takes place. This allows team members to see the presentation, to click links, have [random access](https://twitter.com/paulg/status/838301787086008320) during the presentation,  and to quickly scan the presentation if they missed it. Please also add who the speaker will be to the presentation and the invite. To do this, go to the [GitLab availability calendar](https://calendar.google.com/calendar/embed?src=km839mvmkegodukmn933fp1ss4%40group.calendar.google.com), find the event, click on `more details` and edit the description. People Ops will ping the appropriate team member at the 24-hour mark if the event hasn't been updated yet.
1. Reduce distractions for yourself and the attendees by:
   - having the presentation open in its own new browser window, and only sharing that window in Zoom.
   - switching off notifications (from Slack, email, etc.). On Mac, in the notification center in the upper right hand corner, click on Notifications, select Do Not Disturb (sometimes you need to scroll up to see the option).
1. All calls are published on YouTube, with the exception of Finance, Sales, Security, Channel, and Partnerships. Every call is reviewed prior to publishing to make sure no internal information is shared externally.
1. Right now everyone at GitLab the company is invited to each call, we'll invite the wider community later.
1. Attendance is optional.
1. The update is also announced on and the recording linked from our team call agenda.
1. Tone should be informal, like explain to a friend what happened in the group last month, it shouldn't require a lot of presentation.
1. You can invite someone in the team to give the update, it doesn't need to be the team lead, but the team lead is responsible that it is given.
1. Calls are 5 times a week 30 minutes before the team call, 8:00 to 8:25am Pacific.
1. Calls should be a maximum of 15 minutes (aim for 10 minutes) for the presentation so
  - Please focus on the discussion during the call, not the slide content. **Do not read the contents of your slides.** Everyone is able to read the slide content so the value is in the conversation and questions. Leave time for questions, the presentation is there as warmup to get questions, questions will have the things people care about that you overlooked, they are the most important part of the FGU.
  - People will watch less FGU's if is a bigger time commitment, it is great it they end with time to spare.
  - Save time by writing information on the slides, people can read faster than write, many people will not be able to watch the FGU either live or recorded but can read trough the slides.
  - It is fine if a call ends after 5 or 10 minutes.
  - Do not read the contents on your slides, we already read what is on them before you mention it.
  - People need to be on time for the team call, end no later than :29 and preferably at :25.
  - If there are more questions invite them via a chat channel you mention.
1. Calls are scheduled by an [EA](https://about.gitlab.com/jobs/executive-assistant/).
1. If you need to reschedule, please *switch* your presenting day with another FGU presenter. If you've agreed to switch do the following:
    - Go to the *GitLab Availabiltiy calendar* invite
    - Update the date of your and the other invite to be switched
    - Choose to send an update to the invitees
    - _If prompted_ with the questions to update 1 or all events, choose to only update this event
1. Every group with 5 people or more presents.
1. The call is recorded automatically, and all calls are transferred every hour to a Google Drive folder called "GitLab Videos", which is accessible to all users with a GitLab.com e-mail account.
1. Video recordings will be published on our blog so contributors, users, and customers can see it. We're aiming to publish a blog post once a week of that weeks' recordings with the matching slides in one go.

### Template for the blog post

For instructions on how to create a blog post, please see our [Blog Handbook](https://about.gitlab.com/handbook/marketing/blog/#create-a-new-post).

Please copy the code block below and paste it into a blog post with the file name `yyyy-mm-dd-functional-group-updates.html.md`.

```md
---
title: "GitLab's Functional Group Updates - MMM DD-DD" # replace "MMM" with the current month, and "DD-DD" with the date range
author: Name Surname
author_gitlab: gitlab.com-username
author_twitter: twitter-username
categories: Functional Group Updates
image_title: '/images/blogimages/functional-group-update-blog-cover.jpg'
description: "The Functional Groups at GitLab give an update on what they've been working on"
---

<!-- beginning of the intro - leave it as is -->

Every day from Monday to Thursday, right before our [GitLab Team call](https://about.gitlab.com/handbook/#team-call), a different Functional Group gives an [update](https://about.gitlab.com/handbook/people-operations/functional-group-updates/) to our team.

The format of these calls is simple and short where attendees have access to the presentation content and presenters can either give a quick presentation or jump straight into the agenda and answering questions.

<!-- more -->

## Recordings

All of the updates are recorded using [Zoom](https://zoom.us) at the time of the call. All the recordings will be uploaded to our YouTube account and made public, with the exception of the Sales and Finance updates.

<!-- end of the intro -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### XXX Team

[Presentation slides](link-to-slides-deck)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/8vJBc8MJihE" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### XXX Team

[Presentation slides](link-to-slides-deck)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/8vJBc8MJihE" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

----

Questions? Leave a comment below or tweet [@GitLab](https://twitter.com/gitlab)! Would you like to join us? Check out our [job openings](https://about.gitlab.com/jobs/)!

```

### Uploading and Editing the Videos

- In order to get access to the GitLab YouTube account, follow the instructions in the secure note in 1Password.
- To upload the video, go to YouTube and click the up arrow at the top right corner, next to the GitLab profile picture.
- Change the security level from "public" to either "unlisted" (only those with the link can view) or "private" (only people with access to the GitLab YouTube account can view), so that you can edit the video prior to it being live.
- Under "Basic Info", change the title to follow this pattern: "Functional Group Update XXX-Team Date". Change the description to:

Functional Group Update XXX-Team Date

Presentation - https://example.com [Input your presentation's URL and check if its set to view only for anyone with the link if using Google Slides]

Try GitLab EE FREE for 30 days, no credit card required - https://goo.gl/JdznoY

Want to know more about our free GitLab.com? https://goo.gl/Uol8bl

GitLab License FAQ - https://goo.gl/hZAsYF

Questions?? - https://goo.gl/i7KzQ8


- After the video is done uploading, click "Video Manager" in the bottom right corner.
- Edit the video to start when the meeting actually starts:
  - Click **Edit** next to the video icon.
  - Click the **Enhancements** tab on the top menu bar.
  - Click **Trim** on the bottom right. Slide the left edge of the bar to a few moments before the presentation begins, and the right edge of the bar to a few moments after the presentation ends. Click **Done**.
- Take a screenshot of the second slide of the presentation to make it as custom thumbnail for your video on YouTube. You can upload your custom thumbnail under the **Info and Settings** tab when you are editing a video.
- After the video is finished being edited, change the security level back to "public".
- Make sure to use correct format for [embedding videos from YouTube](/handbook/product/technical-writing/markdown-guide/#display-videos-from-youtube) into the blog post. Replace the video URL only.

## Schedule

There is a rotating schedule:

Week 1:

- Monday:       Discussion                   - Sean
- Tuesday:      Product                      - Job
- Wednesday:    People Ops                   - Barbie
- Thursday:     Finance                      - Paul
- Friday:       Marketing & Sales Development- Courtland

Week 2:

- Monday:       Platform        - Douwe
- Wednesday:    Sales           - Chad
- Thursday:     General         - Sid
- Friday:       Security        - Kathy

Week 3:

- Monday:       UX              - Sarrah Vesselov
- Tuesday:      CI              - Kamil
- Wednesday:    Build           - Marin
- Thursday:     Support         - Lee
- Friday:       Infrastructure  - Eric

Week 4:

- Monday:       Channel         - Michael Alessio
- Tuesday:      Frontend        - Jacob
- Wednesday:    Prometheus      - Ben
- Thursday:     Engineering     - Stan

Week 5:

- Monday:       Marketing       - Joe
- Tuesday:      Edge            - Remy
- Wednesday:    Partnerships    - Eliran
- Thursday:     UX Research     - Sarah O'Donnell
- Friday:       Customer Success- Kristen

## Agenda

A possible agenda for the call is:

1. Accomplishments
1. Concerns
1. Stalled/need help
1. Plans
1. Questions in chat
