---
layout: markdown_page
title: "Hiring"
---

## Hiring pages

- [Interviewing](/handbook/hiring/interviewing/)
- [Vacancies](/handbook/hiring/vacancies/)
- [Jobs](/handbook/hiring/jobs)
- [Lever](/handbook/hiring/lever/)
- [Principles](/handbook/hiring/principles/)

## Related to hiring

- [Benefits](/handbook/benefits/)
- [Visas](/handbook/people-operations/visas/)
- [Compensation](/handbook/people-operations/global-compensation/)
- [Jobs](/jobs/)

## On this page
{:.no_toc}

- TOC
{:toc}

## Definitions

Jobs and vacancies are different things and can't be used interchangeably.

- A [vacancy](/handbook/hiring/vacancies/) is temporary item, posted on Lever, its contents subset of the job, and it is created by copying parts of a job based on an issue.
- A [job](/handbook/hiring/jobs/) is a permanent item, it is maintained under /jobs, its contents is a superset of all vacancies for that job, and it is created with a merge request.

People at GitLab can be a specialist on one thing and expert in many:

- A [specialization](/jobs/specialist/) is specific to a job, each team member can have only one, it defined on the relevant job page.
- An [expertise](/jobs/expert/) is not specific to a a job, each team member can have multiple ones, the expertises a team member has are listed on our team page.

The following parts describe what someone does at GitLab:

1. Level: Senior
1. Job: Developer
1. Specialist: Gitaly specialist
1. Location: EMEA
1. Expert: Reliability, Durability

We use the following terms to refer to a combination of the above:

- Title (level and job, listed on the contract): Senior Developer
- Headline (all parts except expertise, listed on vacancies): Senior Developer, Gitaly specialist, EMEA

Please use the same order as in the examples above, a few notes:

- Level comes before job.
- Specialist comes after job and always includes 'specialist'.
- Location comes after specialization.

## Equal Employment Opportunity

 Diversity is one of GitLab's core [values](https://about.gitlab.com/handbook/values) and
 GitLab is dedicated to providing equal employment opportunities (EEO) to all team members
 and applicants for employment without regard to race, color, religion, gender,
 national origin, age, disability, or genetics. One example of how put this into practice
 is through sponsorship of [diversity events](https://about.gitlab.com/2016/03/24/sponsorship-update/)

 GitLab complies with all applicable laws governing nondiscrimination in employment. This policy applies to all terms and conditions of employment, including recruiting, hiring, placement, promotion, termination, layoff, recall, transfer,
 leaves of absence, compensation, and training. GitLab expressly prohibits any form of workplace harassment.
 Improper interference with the ability of GitLab’s team members to perform their job duties
 may result in discipline up to and including discharge. If you have any complaints, concerns,
 or suggestions to do better please [contact People Ops](/handbook/people-operations/#reach-peopleops).

## Country Hiring Guidelines

Each country has unique rules and regulations affecting a company’s ability to conduct business in that country, as well as the employability of its citizens. These rules can be complex.  Therefore, at this time we are limited in which countries we are able to hire employees and contractors from. Please work with the People Operations team to determine whether we are currently able to hire or contract for employment in the jurisdiction you are seeking.

GitLab has established entities, payroll solutions or partnered with third parties in the following countries:

1. United States
1. The Netherlands
1. United Kingdom
1. Belgium
1. China
1. India
1. Germany
