---
layout: markdown_page
title: "Inbound BDR"
---
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Inbound BDR Handbook

{:toc}

### Onboarding
You will be assigned an onboarding issue by Peopleops. This is a step by step guide/checklist to getting everything in your arsenal set up, such as equipment, tools, security, your Gitlab.com account, as well as specific tools needed for your role. 

### BDR Standards
- Strategize to develop the proper qualifying questions for all types of prospective customers.
- Be able to identify where a prospective customer is in their buying cycle and take appropriate action to help them along their journey towards becoming a customer.
- Faster response time directly influences conversion rates
- Generate Sales Accepted Opportunities (SAOs)
- Inbound to work off of leads within SFDC
- Focus on data integrity and activity logging
- Inbound does not touch any lead that has activity on it within the last 45 days by a different BDR.


### BDR Process

#### Flow
![BDR Process Flow](source/images/handbook/marketing/bdrFlow.png){: .shadow}

It is imperative that you have an understanding of the following concepts to work effectively. Please be sure to review the following resources:

[Glossary](https://about.gitlab.com/handbook/business-ops/#glossary)

[Demand Waterfall](https://about.gitlab.com/handbook/business-ops/customer-lifecycle/)

[Lead Management](https://about.gitlab.com/handbook/business-ops/database-management/#lead-management)

[Rules of Engagement](https://about.gitlab.com/handbook/business-ops/database-management/#rules-of-engagement)

### Researching
Researching leads is an important first step. Not only can you leverage the information to build a rapport, but it will ensure data integrity.

##### Salesforce
*Company*  
- Is there an owned customer/prospect account?
    - Are there any open opportunities?
    - Are there other leads from the organization that are being worked?
    - Who in the organization have we been in contact with? 
    - Can we follow up or reference this?

*Lead*
- Are there any duplicate leads or contacts?
  - Search SFDC for lead name & email address  
- Review Salesforce Activity History
    - Has anyone been in contact with this lead previously?
- Review the widgets built into Salesforce for further information on lead's actions.
    - Clearbit   
    - Lean Data
    - Marketo Sales Insight
        - Interesting Moments
        - Web Activity
        - Emails (sent from Marketo)
        
##### LinkedIn Sales Navigator
*Company*
- Confirm [Sales Segmentation](https://about.gitlab.com/handbook/business-ops/database-management/#sts=Parent Sales Segmentation) by determining the number of potential users.
    - Use all employees with functions of "IT" and "Engineering"
- Summary
   - Does anything stand out that is relevant to their needs as an organization?
   - Have they published any articles that would be worth referencing?

*Lead*
- What is the lead’s role and how does that affect your messaging?
   - Buyer personas
- Previous work
   - Any Gitlab Customers that you can reference?
- Connections
   - Are they connected to anyone at GitLab for a possible introduction?

##### Social Media


##### Company Website
- Mission Statement
- Press Releases/Newsroom
- (ctrl F) search for keywords

### Cadence 

#### Email Structure
- Studies show that Executives read emails in the morning
- Expect to be forwarded to the right person (Always ask)
- Keep emails 90 words or less
- Break into readable stanzas
- Always be specific and tailored
- Make your emails viewable from a mobile device

1. Subject line
2. Preview pain
3. Opening stanza
    - Make it about them, not about you
4. Benefit and value proposition
5. Call to action (CTA)
    - Offer available times for a meeting/call
    - Create urgency

### Call Prospecting
- Call about them, not about you
- Be confident and passionate
- Aim for every role but focus on technical decision makers
- Ask for time
- Focus on your endgame: Sales Accepted Opportunities (SAO)
- Make it easy to say yes
- Obtain a commitment

### Structuring a Prospecting Call
- Introduction
    - Immediately introduce yourself and GitLab
    - Ask for time
- Initial benefit statement
- Qualification
- Inform them/answer questions about GitLab
- Commitments

### Warm Discovery Calls
Purpose: To qualify leads faster and more accurately (Sales Acceptance Criteria) than in an email exchange.

Process: In your reply message to setup/initiate a call, ask a few of your normal BDR questions to prep for the call. To save a step in emails include your Calendly link so leads may schedule the call directly.

    “Hi (lead name), this is (BDR) from GitLab. How are you today?”

    “Great, is now still a good time to talk about (primary issue)?”

After you’ve established the conversation is good to move forward, ask questions and guide the conversation in a way that enables the lead to tell you what their issue/problem is while also providing answers to the sales acceptance criteria. Your primary role is to gather information, and decide more appropriately how to provide assistance and/or qualify the lead for a call with an AE.

    “Great (lead name), thank you for sharing that information. I have a better feel now for how to move forward with your request/issue. I’m going to follow-up with an email recapping what we discussed, send over that documentation I promised and get you in touch with (account executive)”

### Qualifying
Your goal is to generate Sales Accepted Opportunities (SAOs) by gathering all pertinent information needed for an Account Executive to take action. Some examples of sales qualification questions can be found [here](/handbook/sales-qualification-questions/).

### Miscellaneous

#### BDR & BDR Team Lead
##### Formal Weekly 1:1
- Mental check-in (winning and success)
- Coaching - email strategy, tools, campaigns, cadence, best practices
- Review goals at the account level and personal level
- Strategy for next week
- Upcoming events/campaigns that can be leveraged
- Personal goals and commitments

### Measurements

#### Variable Compensation
Full-time Inbound BDRs have a significant portion of their pay based on performance and objective attainment. Performance based "bonuses" are based on quota attainment.

Actions for obtaining results will be prescribed and measured, but are intentionally left out of bonus attainment calculations to encourage experimentation. Inbound BDRs will naturally be drawn to activities that have the highest yield, but freedom to choose their own daily activities will allow new, higher yield activities to be discovered.

- Team and individual quotas are based on GitLab's revenue targets
- Quotas will be made known by having each BDR sign a participant form that clearly lays out quarterly quotas that match the company's revenue plan
- Bonuses are paid quarterly.
- Bonuses are based solely on sales accepted opportunities generated. Guidelines for a [sales accepted opportunity](/handbook/marketing/marketing-sales-development/sdr/#acceptedopp)
- A new BDR's first month's bonus is typically based on completing onboarding

#### Additional Measurements
BDRs will also be measured on the following:

* Results
  * Pipeline value of BDR generated opportunities
  * IACV won from opportunities BDR generated
* Activity
  * Number of opportunities created
  * Number of emails sent
  * Number of calls made

Note, while important, the above measurements do not impact your quota attainment. Your main focus will be achieving your SAO quota.

