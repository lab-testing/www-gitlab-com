---
layout: markdown_page
title: "Paid time off at GitLab"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Paid Time Off

Time away from work can be extremely healthy to maintain a work/life balance. GitLab encourages managers and leadership to set the example of taking time off when you need it, and ensuring their reports do the same.

For example, our CEO, went on a wonderful vacation to Hawaii with his significant other for a week.  

A service engineer remarked that “In the 3 months I've been at GitLab, I've taken more time off than the last 2 years at my previous job.”

Another great example of taking time off is a GitLabber taking the day to do some spring cleaning, because vacations are not always to exotic places, but instead might be taking some time for themselves, at home or abroad.

It is so important to take time off to recharge batteries and refresh the mind so you can come back to GitLab with new energy and be prepared to do your best work ever!

If you are taking leave of more than 25 calendar days for Parental Leave, please see our [Parental Leave Policy](https://about.gitlab.com/handbook/benefits/#parental-leave).

## A GitLabber's Guide to Time Off

As we all work remotely it can sometimes be difficult to know when and how to plan time off. Please find some advice and guidance below on how this can be done in a collaborative and easy way.

1. We have a "no ask" time off policy. This means that:
    * You do not need to ask permission to take time off unless you want to have more than 25 consecutive calendar days off. The 25 days limit for no ask is per vacation, not per year. You can do multiple no ask vacations per year that are more than 25 days in total, there is no limit to this. What we care about is that you get results, not how long you work. You are also welcome to take more than 25 days off in one stretch, just make sure to discuss with your manager upfront.
    * Always make sure that your job responsibilities are covered while you are away.
    * We strongly recommended to take at least a minimum of 2 weeks of vacation per year, if you take less your manager might follow up with you to discuss what is wrong.
1. We don't frown on people taking time off, but rather encourage that people take care of themselves and others by having some time away. Especially if you notice that your co-worker is working long hours over a sustained period.
1. Working hours are flexible, you are invited to the [team call](#team-call) if you are available, and if you want you can post to the #working-on chat channel what is on your mind so others can offer suggestions.
1. You don't need to worry about taking time off to go to the gym, [take a nap](https://m.signalvnoise.com/sleep-deprivation-is-not-a-badge-of-honor-f24fbff47a75), go grocery shopping, doing household chores, helping someone, taking care of a loved one, etc. If something comes up or takes longer than expected and you have urgent tasks and you're able to communicate, just ensure the rest of the team **knows** and someone can pick up any urgent tasks.
1. GitLab encourages team members to volunteer within their community to take care of others.
1. Add an **appointment** to the GitLab availability calendar well in advance of your plans (if possible), you can always change it later. Invite your manager to the calendar event so that they know you will be away. When you leave, you can also add your [status in Slack](https://about.gitlab.com/handbook/tools-and-tips/#slack-status)
1. In case it can be useful add your planned time off as a **FYI** on the next agenda of the team call.
1. You do need to ensure that not more than **half** of the people that can help with availability emergencies (the on-call heroes), regular support, sales, or development are gone at any moment. You can check for this on the availability calendar, so be sure to add time off early.
1. Being part of a global remote team means you need to be highly organized and a considerate team player. Each team has busy times so it is always a good idea to check with them to ensure there is adequate coverage in place.
1. Please see the [On-Call](https://about.gitlab.com/handbook/on-call/)
page for information on how to handle scheduled leave for someone from the [on-call](#on-call) team.
1. We will **help** clients during official days off, unless they are official days off in both the Netherlands and the U.S. We try to have people working who are in a country that don't have an official day off. If you need to work during an official day off in your country, you should take a day off in return.
1. Please also remember to turn on your out of office message and include contact details of a co-worker in case anything urgent or critical comes into your inbox while you're away. If you have to respond to an incident while being on-call outside of your regular working hours, you should feel free to take off some time the following day to recover and be well-rested. If you feel pressure to _not_ take the time off to rest, refer to this part of the handbook and explain that you had to handle an incident.

### Management’s Role in Paid Time Off

Managers have a duty of care towards their direct reports in managing their wellbeing and ensuring that time off is being taken. When working from home remotely, at times the work life balance can be difficult to find. It is also easy to forget that your team is working across multiple timezones, some may feel obligated to work longer to ensure there is overlap. It is important that you check-in with your reports at your one-to-ones and if you think someone needs some time off let them know they can do this.

If you discover that multiple people in your team want to be off at the same time, use good judgement, see what the priorities are and review the impact to the business. Discuss this with your team so you can manage the time off together. It is also a good idea to remind your team to give you and the rest of the team an early heads-up, if possible, about upcoming vacation plans.

### Recognizing Burnout

It is important for us to take a step back to recognize and acknowledge the feelings of being "burned out."
We are not as effective or efficient when we work long hours, miss meals or forego nurturing our personal lives
for sustained periods of time. If you feel yourself or notice someone on your team experiencing [burnout](http://www.mayoclinic.org/healthy-lifestyle/adult-health/in-depth/burnout/art-20046642), be sure to address it right away.

To get ahead of a problem, be sure to communicate with your manager if one of the following statements ever applies to you:

* "I am losing interest in social interaction." - This is especially dangerous in a remote-only setting.
* "I've lost the motivation to work." - Everyone has days when they don't want to work
but if you hear yourself saying this often, you're on the road to burnout.
* "I feel tired often." - Indicative of being overworked for prolonged periods of time.
* "I get easily agitated."
* "I've been hostile to my coworkers." - You see yourself "snap" at people for
no apparent reason.
* "I've been having headaches often." - A headache can manifest itself for multiple
reasons but if you catch yourself only having headaches on work days, it is time to
evaluate your position.

If you, your peers or your direct reports are showing signs of burnout, you should take time out to focus on things that relax you and improve your overall health and welfare.

As a manager, it should be your task to evaluate your team's state of mind.
Address burnout by discussing options with your team member to manage contributing stressors and evaluate the workload. Few tips:

* Try to follow each of your team member work habits. If they start being less efficient,
and investing more hours, they might be on a road to burnout.
* Try to keep in mind when they had their last day off. If they hadn't had a personal day
in a long time, look closer at their behaviours.
* Make sure you let your team members know they can talk to you about their challenges.

Other [tips to avoid burnout](http://www.mayoclinic.org/healthy-lifestyle/adult-health/in-depth/burnout/art-20046642?pg=2) include:
* Assess and pursue your interests, skills and passions.
* Take breaks during the day to eat healthy foods and stretch your legs. [Timeout app](http://dejal.com/timeout/) can help with that.
* Make time each day to increase blood and oxygen circulation which improves brain activity and functionality.
* Get plenty of restful sleep.
* Meditate to take your mind away from work. [Headspace](https://www.headspace.com/science) and [Calm](https://www.calm.com/meditate) are good tools for creating meditation
habits.
* Don't start with work as soon as you wake up. Take your time doing your morning routine.
* Set yourself as away when you are not working. Snooze your Slack notifications. It is
fine to be not reachable all the time.

Don't let burnout creep up on you. Working remotely can allow us to create bad habits, such as working straight through lunch to get something finished. Once in a while this feels good to check that nagging task or big project off the list but don't let this behavior become a bad habit. Before long, you'll begin to feel the effects on your body and see it in your work.

Keep in mind that you are not alone! Chances are that you have a colleague who already
experienced burnout or have been on the road to burnout. Schedule coffee calls with
your team members or with the people you like. Talk to your manager. If none of that
is an option for you, schedule a coffee call with [Marin](slack://user?team=T02592416&id=U025925R0).

Take care not to burn yourself out!

### Statutory Vacation Requirements

The following is a list of all statutory annual vacation entitlements by entity and country. Once the statutory amounts have been taken, employees can still make use of GitLab's unlimited leave policy.

1. GitLab LTD (UK Employees)
    * Employees must take 20 vacation days. The days will accrue from the start date. There is no carryover for unused vacation days.
1. GitLab BV (Netherlands Employees)
    * Employees must take 20 vacation days. Any used days will be carried into the next calendar year, but expire after six months. All days will be posted upon start.
1. GitLab BV (Belgium Employees)
    * Employees must take 20 vacation days; the days taken must be communicated to the Belgian payroll provider each month by People Ops. These days do not carry over into the next calendar year.
1. GitLab BV (Contractors)
    * Contractors do not have statutory vacation requirements, but are eligible for our Unlimted Time off Policy.
1. GitLab GmbH (Germany Employees)
    *  Employees must take 20 vacation days. The days will accrue from the start date. In general, employees must take their annual vacation days during the calendar year. Otherwise, it is forfeited. However, unused holiday can be carried forward until the 31st of March of the next calendar year if the employee was unable to take the holiday due to operational or personal reasons.
1. GitLab INC (US Employees)
    * The U.S. [Fair Labor Standards Act (FLSA)](https://www.dol.gov/general/topic/workhours/vacation_leave) does not require payment for time not worked, such as vacations, sick leave or federal or other holidays.
1. GitLab Inc. (China)
    * Employees worked for one-year but less than 10 years, the annual leave is 5 days; worked for more than 10 years but less than 20 years, the annual leave is 10 days;  worked for more than 20 years, the annual leave is 15 days.
1. LYRA (India)
    * The statutory requirements of India are covered through our Unlimited Time off Policy.

### Processing Vacation Requirements
1. People Ops will export the availability calendar monthly using the "Shared Calendar" sheet on the google drive, and add any applicable entries to BambooHR under Time Off. Employees with an accrual in BambooHR should review the entries from People Ops to ensure accuracy.  
1. Each June, People Ops will need to review the BV (Netherlands) Accrual for all employees and remove any carry over from the previous calendar year that was not used.
1. In January, People Ops will adjust any negative carryover for all accruals in BambooHR back to zero for the year. To do this go to Time Off, Adjust Balance, and add any extra days.
