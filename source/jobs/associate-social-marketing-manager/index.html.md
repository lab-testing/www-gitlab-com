---
layout: job_page
title: "Associate Social Marketing Manager"
---

The world’s best brands feel more like a friend to their audience than a company. This is because the world’s best brands understand people, and weave that understanding into everything they do. The associate social marketing manager is a key part of building GitLab’s brand into one that is loved by its audience of developers, IT ops practitioners, and IT leaders. 

This role will focus on understanding people who make up the GitLab community, including those who use GitLab (or are potential users), sharing content over social channels that they’ll find useful and valuable, and helping the rest of the marketing department understand the people that make up the markets that GitLab is serving.

## Responsibilities
- Own strategic social media planning in support of product launches, company news, crisis communications, and brand campaigns by determining what to publish, when, and where.
- Work with the team to ensure social amplification and engagement is incorporated as part of integrated campaign strategy and execution
- Be the voice of our consumers to our internal teams. Advocate for their needs, represent their feedback, and flag opportunities to do more, and identify potential pitfalls
- Grow GitLab’s social fan base while ensuring the quality of our followers and the relationships we've built with them on Twitter, LinkedIn, Instagram, Facebook, and Google+
- Identify key social influencers and work to develop and cultivate relationship and engagement.
- Ensure day-to-day publishing, reporting, and strategies are in place
- Manage the social calendar and craft social campaigns that will help engage, grow and educate our followers
- Social listening: Monitor conversations about GitLab online to flag positive brand opportunities that can be converted into meaningful marketing materials via offline activities and user-generated content (blog posts, speaking opps, interviews, etc.) 
- Assist Community Advocates in flagging negative conversations and brand associations 
- Handle reporting for social media initiatives, including channel health, campaign, and launch reports

## Requirements
- In-depth knowledge and understanding of social media platforms and their respective uses including Twitter, Facebook, LinkedIn, YouTube, and Google+
- Excellent writing and communication skills
- Highly collaborative. This role will require effective collaboration across multiple teams, organizations, and individuals. 
- Extremely detail-oriented and organized 
- Ability to confidently write and engage in the GitLab brand voice and personality in real time
- You share our [values](https://about.gitlab.com/handbook/values/), and work in accordance with those values
- BONUS: A passion and strong understanding of the industry and our mission

## Hiring Process
Applicants for this position can expect the hiring process to follow the order below. Please keep in mind that applicants can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](https://about.gitlab.com/team/).

- Qualified applicants will be invited to schedule a [screening call](https://about.gitlab.com/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.
- Next, candidates will be invited to schedule a series of 45 minute interviews with our Content Marketing Manager, Content Editor, and Community Advocate
- Candidates will then be invited to schedule 45-minute interviews with our Senior Director of Marketing and Sales Development and our CMO.
- Finally, our CEO may choose to conduct a final interview.
- Successful candidates will subsequently be made an offer via email.

Additional details about our process can be found on our [hiring page](https://about.gitlab.com/handbook/hiring/).

## Apply

Please note that if we are actively hiring for a position, you will see it listed on our [jobs page](https://about.gitlab.com/jobs/), where all of our current openings are advertised. To apply, please click on the name of the role you are interested in, which will take you to our applicant tracking system (ATS), [Lever](https://www.lever.co/).

Avoid the [confidence gap](https://www.theatlantic.com/magazine/archive/2014/05/the-confidence-gap/359815/
); you do not have to match all the listed requirements exactly to apply. Our hiring process is described in more detail in our [hiring handbook](https://about.gitlab.com/handbook/hiring/).

## About GitLab

GitLab Inc. is a company based on the GitLab open-source project. GitLab is a community project to which over 1,000 people worldwide have contributed. We are an active participant in this community, trying to serve its needs and lead by example. We have one [vision](https://about.gitlab.com/strategy/): everyone can contribute to all digital content, and our mission is to change all creative work from read-only to read-write so that everyone can contribute.

We [value](https://about.gitlab.com/handbook/values/) results, transparency, sharing, freedom, efficiency, frugality, collaboration, directness, kindness, diversity, boring solutions, and quirkiness. If these values match your personality, work ethic, and personal goals, we encourage you to visit our [primer](https://about.gitlab.com/primer/) to learn more. Open source is our culture, our way of life, our story, and what makes us truly unique.

Top 10 reasons to work for GitLab:

1. Work with helpful, kind, motivated, and talented people.
1. Work remote so you have no commute and are free to travel and move.
1. Have flexible work hours so you are there for other people and free to plan the day how you like.
1. Everyone works remote, but you don't feel remote. We don't have a head office, so you're not in a satellite office.
1. Work on open source software so you can interact with a large community and can show your work.
1. Work on a product you use every day: we drink our own wine.
1. Work on a product used by lots of people that care about what you do.
1. As a company we contribute more than we take, most of our work is released as the open source GitLab CE.
1. Focused on results, not on long hours, so that you can have a life and don't burn out.
1. Open internal processes: know what you're getting in to and be assured we're thoughtful and effective.

See [our culture page](https://about.gitlab.com/culture) for more!

Work remotely from anywhere in the world. Curious to see what that looks like? Check out our [remote manifesto](https://about.gitlab.com/2015/04/08/the-remote-manifesto/).

