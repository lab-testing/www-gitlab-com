---
layout: job_page
title: "Engineering Manager - Production"
---

The Engineering Manager, Production directly manages the developers that run GitLab.com. They see the team as their product. They work on small features or bugs to keep their technical skills sharp and stay familiar with the code. But they emphasize hiring a world-class team and putting them in the best position to succeed. They own the delivery of product commitments and they are always looking to improve the productivity of their team. They must also coordinate across departments to accomplish collaborative goals.

## Responsibilities

- Hire an incredible team that lives our [values](https://about.gitlab.com/handbook/values/)
- Improve the happiness and productivity of the team
- Work on small features and bugs (nothing critical path)
- Manage the agile development and continuous delivery process
- Hold regular 1:1's with team members
- Manage agile projects
- Work across sub-departments within engineering
- Improve the quality, security and performance of the product

## Requirements

- 2-5 years managing software engineering teams
- Demonstrated teamwork in a peak performance organization
- Kubernetes, Docker, Go, or Linux administration
- Experience running a consumer scale platform
- Product company experience
- Enterprise software company experience
- Computer science education or equivalent experience
- Passionate about open source and developer tools
- Exquisite communication skills

## Nice-to-have's

- Online community participation
- Remote work experience
- Startup experience
- Significant open source contributions

## Hiring Process

Applicants for this position can expect the hiring process to follow the order below. Please keep in mind that applicants can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/team).

* Selected candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/#screening-call) with our Recruiting team
* Next, candidates will be invited to schedule a 45 minute first interview with our VP of Engineering
* Next, candidates will be invited to schedule a 45 minute second peer interview with either our Director of Security or Director of Quality
* Next, candidates will be invited to schedule a 45 minute third interview with an Engineering team member
* Finally, candidates may be asked to schedule a 50 minute final interview with our CEO
* Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](/handbook/hiring).

## Apply

Please note that if we are actively hiring for a position, you will see it listed on our [jobs page](https://about.gitlab.com/jobs/), where all of our current openings are advertised. To apply, please click on the name of the role you are interested in, which will take you to our applicant tracking system (ATS), [Lever](https://www.lever.co/).

Avoid the [confidence gap](https://www.theatlantic.com/magazine/archive/2014/05/the-confidence-gap/359815/
); you do not have to match all the listed requirements exactly to apply. Our hiring process is described in more detail in our [hiring handbook](https://about.gitlab.com/handbook/hiring/).

## About GitLab

GitLab Inc. is a company based on the GitLab open-source project. GitLab is a community project to which over 1,000 people worldwide have contributed. We are an active participant in this community, trying to serve its needs and lead by example. We have one [vision](https://about.gitlab.com/strategy/): everyone can contribute to all digital content, and our mission is to change all creative work from read-only to read-write so that everyone can contribute.

We [value](https://about.gitlab.com/handbook/values/) results, transparency, sharing, freedom, efficiency, frugality, collaboration, directness, kindness, diversity, boring solutions, and quirkiness. If these values match your personality, work ethic, and personal goals, we encourage you to visit our [primer](https://about.gitlab.com/primer/) to learn more. Open source is our culture, our way of life, our story, and what makes us truly unique.

Top 10 reasons to work for GitLab:

1. Work with helpful, kind, motivated, and talented people.
1. Work remote so you have no commute and are free to travel and move.
1. Have flexible work hours so you are there for other people and free to plan the day how you like.
1. Everyone works remote, but you don't feel remote. We don't have a head office, so you're not in a satellite office.
1. Work on open source software so you can interact with a large community and can show your work.
1. Work on a product you use every day: we drink our own wine.
1. Work on a product used by lots of people that care about what you do.
1. As a company we contribute more than we take, most of our work is released as the open source GitLab CE.
1. Focused on results, not on long hours, so that you can have a life and don't burn out.
1. Open internal processes: know what you're getting in to and be assured we're thoughtful and effective.

See [our culture page](https://about.gitlab.com/culture) for more!

Work remotely from anywhere in the world. Curious to see what that looks like? Check out our [remote manifesto](https://about.gitlab.com/2015/04/08/the-remote-manifesto/).
